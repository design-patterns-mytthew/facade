package eu.mytthew;

class Pump {
	private boolean working;

	void turnOn() {
		working = true;
	}

	void turnOff() {
		working = false;
	}

	boolean isWorking() {
		return working;
	}

}
