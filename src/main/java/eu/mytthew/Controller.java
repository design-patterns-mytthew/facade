package eu.mytthew;

public class Controller {
	private Engine engine;
	private Pump pump;
	private Valve valve;

	public Controller() {
		engine = new Engine();
		pump = new Pump();
		valve = new Valve();
	}

	public void open() {
		pump.turnOn();
		engine.setLevel(10);
		valve.open();
	}

	public void close() {
		pump.turnOff();
		engine.setLevel(-1); //won't it crash?
		valve.close();
	}
}
