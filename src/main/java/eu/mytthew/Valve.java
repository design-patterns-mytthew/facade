package eu.mytthew;

class Valve {
	private boolean opened;

	void open() {
		opened = true;
	}

	void close() {
		opened = false;
	}

	boolean isOpened() {
		return opened;
	}

}
